# GitLab CI: Specific Runner
---

## Install GitLab Runner on Windows

### Installation Steps
- Create a folder , ex.: C:\GitLab-Runner
- Download the binary for [x86](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe) or [amd64](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe) and put it into the folder you created. Rename the binary to gitlab-runner.exe


#### Registering the Specific Runner
- Grab the shared-Runner token on the youtgitlaburl/admin/runners page
To register a Runner under Windows, Run the following commands (*having an Administrator previlege*):
   ```shell
   cd gitlab-runner
   gitlab-runner register
   ```
1. Enter your GitLab instance URL:
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com ) https://gitlab.com
3. Enter the token you obtained to register the Runner
4. Enter a description for the Runner
5. Enter the tags associated with the Runner, you can change this later in GitLab’s UI
6. Enter the Runner executor

#### Installing  and starting the runner
```shell
gitlab-runner install
gitlab-runner start
```
#### Verify the  the Setup
Go to your specific *project -> Click on the last symbol ->CI/CD Setting* . You will see the tag of the registered runner.